from distutils.core import setup

setup(name='neon',
      version='0.9.3',
      description='A simple image viewer',
      author='Valery Febvre',
      author_email='vfebvre@easter-eggs.com',
      url='http://neon.projects.openmoko.org',
      classifiers=[
        'Development Status :: 2 - Beta',
        'Environment :: X11 Applications',
        'Intended Audience :: End Users/Phone UI',
        'License :: GNU General Public License (GPL)',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Topic :: Desktop Environment',
        ],
      data_files=[('share/applications', ['neon.desktop']),
                  ('share/pixmaps', ['neon.png']),
                  ('share/neon', ['README']),
                  ('share/neon/themes', ['neon.edj']),
                  ],
      scripts = ['neon.py']
      )
