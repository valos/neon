## Description
Neon is a simple image viewer.
It's designed to be lightweight, fast, and easy to use.
It's written in Python/EFL.

https://gitlab.com/valos/neon


## Authors
Valéry Febvre <vfebvre@easter-eggs.com>


## Requirements
- python-evas
- python-ecore
- python-edje


## Known Issues
- Filesystem browser needs a scroll bar.
- Scrolling in filesystem browser is slow with stable kernel 2.6.24
  and acceptable with Andy's kernel 2.6.28.


## Changelog

### 2008-12-18 - Version 0.9.3
- Added a rotation tool

### 2008-12-16 - Version 0.9.2
- Fixed a bug in window resizing
  Concretely, after a rotation of the screen, we got menus, buttons and
  part of the image all overlapping.

### 2008-12-16 - Version 0.9.1
- Added a fullscreen toggle button

### 2008-12-14 - Version 0.9
- First version
- Basic features/gui
