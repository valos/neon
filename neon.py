#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Neon -- A simple image viewer
#
# Copyright (C) 2008 Valéry Febvre <vfebvre@easter-eggs.com>
# http://neon.projects.openmoko.org/
#
# This file is part of Neon.
#
# Neon is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Neon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Neon is a simple image viewer
It's designed to be lightweight, fast, and easy to use.
"""

APP_NAME = 'Neon'
WIN_WIDTH  = 480
WIN_HEIGHT = 640
BAR_HEIGHT = 96
FSB_NAVBAR_HEIGHT = BAR_HEIGHT + 14
#EDJE_FILE = '/usr/share/neon/themes/neon.edj'
EDJE_FILE = 'neon.edj'

import sys, os, time
import evas, ecore, ecore.evas, edje

# Slightly modified version of KineticList class used in 03-kinetic_list example of python-edje
class NeonFsb(evas.ClippedSmartObject):
    def __init__(self, gui):
        self.gui = gui
        evas.ClippedSmartObject.__init__(self, self.gui.canvas)

        self.elements = []
        self.objects = []
        self.image_objects = []
        self.path = None
        self.backwards = []
        self.forwards = []
        self.w, self.h = (None, None)
        self.realized = False

        self.top_pos = 0
        self.last_top_pos = 0
        self.last_start_row = -1

        self.row_width = None
        self.row_height = None
        self.file_selected = None

        self.navbar = edje.Edje(self.gui.canvas, file=EDJE_FILE, group="fsb-navbar")
        self.navbar.part_object_get('backward').on_mouse_down_add(self.__on_navbar_action, 'backward')
        self.navbar.part_object_get('forward').on_mouse_down_add(self.__on_navbar_action, 'forward')
        self.navbar.part_object_get('up').on_mouse_down_add(self.__on_navbar_action, 'up')
        self.navbar.part_object_get('home').on_mouse_down_add(self.__on_navbar_action, 'home')
        self.navbar.part_object_get('exit').on_mouse_down_add(self.gui.quit)
        self.navbar.part_object_get('fullscreen').on_mouse_down_add(self.gui.toggle_fullscreen)
        self.navbar.size = (WIN_WIDTH, FSB_NAVBAR_HEIGHT)

        self.mouse_down = False
        self.last_y_pos = 0
        self.start_pos = 0
        self.mouse_moved = False
        self.continue_scrolling = False
        self.is_scrolling = False
        self.do_freeze = False

    def clear(self):
        self.elements = []

        self.top_pos = 0
        self.last_top_pos = 0
        self.last_start_row = -1
        self.last_y_pos = 0
        self.mouse_moved = False
        self.continue_scrolling = self.is_scrolling = False

    def freeze(self):
        self.do_freeze = True

    def load(self, path, first=True):
        if not os.access(path, os.R_OK):
            return

        self.path = path

        self.freeze()
        if not first:
            self.clear()
        self.images = []
        files = os.listdir(path)
        files.sort()
        for file in files:
            file_path = os.path.join(path, file)
            if os.path.isfile(file_path):
                sfile = file.split('.')
                sfile.reverse()
                ext = sfile[0]
                if ext.lower() in ['gif', 'jpg', 'jpeg', 'png', 'tiff', 'xpm']:
                    self.images.append(file_path)
                    #print 'IMG', file
                    self.row_add(file, None)
            else:
                if file[0] != '.':
                    #print 'DIR', file
                    self.row_add(file, None)
        self.thaw()
        if first:
            self.gui.edje_main.part_swallow("fsb-list", self)

        title = '%s (%d items)' % (path, len(self.elements))
        self.navbar.part_object_get("title").text_set(title)

    def __open_file(self, filename):
        file_path = os.path.join(self.path, filename)
        if os.path.isfile(file_path):
            # open image
            self.hide()
            self.gui.viewer.img.index = self.images.index(file_path)
            self.gui.viewer.img.load(file_path)
        else:
            # open folder
            if self.path:
                self.backwards.append(self.path)
            self.forwards = []
            self.load(file_path, False)

    def __on_navbar_action(self, icon, ev, action):
        if action in ['home', 'up']:
            if action == 'home':
                path = os.path.expanduser("~")
            elif action == 'up':
                if self.path == '/':
                    return
                path = '/'.join(self.path.split('/')[:-1]) or '/'
            self.forwards = []
            if self.path:
                self.backwards.append(self.path)
        elif action == 'backward':
            if len(self.backwards) == 0:
                return
            path = self.backwards.pop()
            self.forwards.append(self.path)
        elif action == 'forward':
            if len(self.forwards) == 0:
                return
            path = self.forwards.pop()
            self.backwards.append(self.path)
        self.load(path, False)

    def thaw(self):
        self.do_freeze = False
        if self.realized:
            self.__update_variables_after_new_elements()
            self.__update_screen()

    def hide(self):
        self.navbar.hide()
        for obj in self.objects:
            obj.hide()

    def show(self):
        self.navbar.show()
        self.__update_screen()

    def row_add(self, label, image):
        self.elements.append((label, image))

        if not self.do_freeze:
            self.__update_variables_after_new_elements()
            self.__update_screen()

    def __on_mouse_move(self, edje_obj, emission, source, data=None):
        if self.mouse_down:
            x_pos, y_pos = self.gui.canvas.pointer_canvas_xy

            if abs(self.start_pos - y_pos) < self.row_height:
                return

            diff = int(self.last_y_pos - y_pos)
            if diff == 0:
                return

            self.mouse_moved = True

            # Reset the data if the direction of the mouse move is changed
            if self.last_diff != -1 and (diff < 0) != (self.last_diff < 0):
                self.last_y_pos = y_pos
                self.start_pos = y_pos
                self.start_time = time.time()

            self.last_diff = diff
            self.top_pos += diff

            self.last_y_pos = y_pos
            self.__update_screen()
            self.last_update_time = time.time()

    def __on_mouse_down(self, edje_obj, emission, source, data=None):
        if self.mouse_down:
            return
        if not self.is_scrolling:
            self.mouse_moved = False

        self.continue_scrolling = False
        self.mouse_down = True

        x_pos, y_pos = self.gui.canvas.pointer_canvas_xy

        self.last_diff = -1
        self.last_y_pos = y_pos

        self.file_selected = edje_obj.data['filename']

        self.start_pos = y_pos
        self.start_time = time.time()
        self.last_update_time = time.time()

    def __on_mouse_up(self, edje_obj, emission, source, data=None):
        if self.mouse_down:
            self.mouse_down = False

            x_pos, end_pos = self.gui.canvas.pointer_canvas_xy

            if not self.mouse_moved and not self.is_scrolling and self.file_selected:
                for obj in self.objects:
                    if "filename" in obj.data.keys():
                        if obj.data["filename"] == self.file_selected:
                            obj.signal_emit("selected", "")
                        else:
                            obj.signal_emit("unselected", "")
                ecore.timer_add(.1, self.__open_file, self.file_selected)
                self.file_selected = None
                return

            self.mouse_moved = False
            self.is_scrolling = False

            # do not scroll automatically if the finger was paused
            if time.time() - self.last_update_time > 0.1:
                return

            end_time = time.time()

            pos_diff =  end_pos - self.start_pos
            time_diff = end_time - self.start_time

            self.pixel_per_sec = pos_diff / time_diff
            self.continue_scrolling = True
            ecore.animator_add(self.__do_scroll)

    def __do_scroll(self):
        self.is_scrolling = True

        if self.continue_scrolling == False:
            return False

        diff = int(self.pixel_per_sec / 10)

        if abs(self.pixel_per_sec) - diff <= self.row_height:
            offset = self.top_pos % self.row_height

            if offset >= self.row_height / 2:
                self.sign = 1
                offset = self.row_height - offset
            else:
                self.sign = -1

            self.pixels_left = offset

            ecore.animator_add(self.__do_magnetic_scroll)
            return False

        if diff != 0:
            self.top_pos -= diff
            self.pixel_per_sec -= self.pixel_per_sec / 10
            self.__update_screen()

        return True

    def __do_magnetic_scroll(self):
        if self.pixels_left <= 0 or abs(self.pixel_per_sec) < 1:
            self.mouse_moved = False
            self.is_scrolling = False
            return False

        self.pixel_per_sec -= (self.pixel_per_sec / 10)

        pixels_to_substract = int(abs(self.pixel_per_sec / 10))
        if abs(pixels_to_substract) < 1:
            pixels_to_substract = 1

        if self.pixels_left - pixels_to_substract > 0:
            self.pixels_left -= pixels_to_substract
            self.top_pos += self.sign * pixels_to_substract
        else:
            self.top_pos += self.sign * self.pixels_left
            self.pixels_left = 0

        self.__update_screen()
        return True

    def __manage_objects(self):
        remain = (self.h % self.row_height) > 1
        needed_objects = ((self.h / self.row_height) + 1 + remain)
        current_objects = len(self.objects)

        if current_objects < needed_objects:
            for i in range(current_objects, needed_objects):
                obj = edje.Edje(self.gui.canvas)
                obj.file_set(EDJE_FILE, "fsb-list-item")

                obj.signal_callback_add("mouse,move", "*", self.__on_mouse_move)
                obj.signal_callback_add("mouse,down,1", "*", self.__on_mouse_down)
                obj.signal_callback_add("mouse,up,1", "*", self.__on_mouse_up)

                obj.size = (self.row_width, self.row_height)
                obj.clip = self
                self.objects.append(obj)

                image_obj = self.evas.Image()
                image_obj.size = (100, 75)
                obj.part_swallow("thumbnail", image_obj)
                self.image_objects.append(image_obj)

        elif needed_objects < current_objects:
            for i in range(needed_objects, current_objects):
                pass # Make this work, it throws exception that makes
                     # things stop working properly
                #del self.objects[i]

    def __update_variables_after_resize(self):
        remainer = (self.h % self.row_height) > 0
        self.max_visible_rows = (self.h / self.row_height) + 1
        self.max_visible_elements = self.max_visible_rows

        # Invalidate variable in order to repaint all rows
        # Some might not have been painted before (Didn't
        # fit on the screen
        self.last_start_row = -1

        self.__update_variables_after_new_elements()

    def __update_variables_after_new_elements(self):
        if not self.realized:
            return

        self.min_pos = 0
        remainer = (self.h % self.row_height) > 0
        self.row_amount = len(self.elements) + 1
        if self.row_amount > self.max_visible_rows:
            self.max_pos = self.row_height * (self.row_amount - self.max_visible_rows)
            if remainer:
                self.max_pos -= self.h % self.row_height
        else:
            self.max_pos = 0

        #print "\nMax visible rows = ", self.max_visible_rows
        #print "Row amount = ", self.row_amount
        #print "Remain = ", remainer, self.h % self.row_height
        #print "Max pos = ", self.max_pos

    def __update_screen(self):
        if self.top_pos > self.max_pos:
            self.top_pos = self.max_pos

        remainer = (self.h % self.row_height) > 0
        row_offset = (self.top_pos / self.row_height)
        pixel_offset = -(self.top_pos % self.row_height)
        start_row = row_offset
        end_row = self.max_visible_rows + row_offset

        SCROLL_DOWN = self.top_pos > self.last_top_pos
        SCROLL_UP = self.top_pos < self.last_top_pos

        # Let's not move over the last element
        if SCROLL_DOWN and self.last_top_pos >= self.max_pos:
            #print "Let's not move over the last element", self.last_top_pos, self.max_pos
            self.top_pos = self.max_pos
            self.last_top_pos = self.top_pos
            self.continue_scrolling = False
            return

        # Let's not move over the first element
        if SCROLL_UP and self.last_top_pos <= self.min_pos:
            #print "Let's not move over the first element", self.last_top_pos, self.min_pos
            self.top_pos = self.min_pos
            self.last_top_pos = self.top_pos
            self.continue_scrolling = False
            return

        # Overflow scrolling down
        if SCROLL_DOWN and end_row > self.row_amount:
            offset = end_row - self.row_amount
            end_row -= offset
            start_row -= offset
            row_offset -= offset
            self.top_pos = self.max_pos
            pixel_offset = 0

        # Overflow scrolling up
        if SCROLL_UP and start_row < 0:
            self.top_pos = self.min_pos
            end_row -= start_row
            start_row = 0
            row_offset = 0
            pixel_offset = 0

        self.last_top_pos = self.top_pos

        if start_row != self.last_start_row:
            for i in range(0, len(self.objects)):
                self.objects[i].hide()

        for i in range(start_row, end_row):
            obj_iter  = i - start_row
            data_iter = i

            try:
                label, image = self.elements[data_iter]
            except Exception, e:
                break

            x = self.top_left[0]
            y = self.top_left[1] + self.row_height * (i - row_offset) + pixel_offset

            self.objects[obj_iter].move(x, y)

            if start_row != self.last_start_row:
                #self.image_objects[obj_iter].file_set(image)
                self.objects[obj_iter].part_text_set("label", "%d - %s" % (i + 1, label))
                self.objects[obj_iter].data["filename"] = label
                self.objects[obj_iter].signal_emit("unselected", "")

            self.objects[obj_iter].show()
        self.last_start_row = start_row

    def resize(self, w, h):
        self.w = w
        self.h = h

        self.row_width  = self.w
        self.row_height = self.h / 5

        self.navbar.size = (self.w, FSB_NAVBAR_HEIGHT)

        self.__manage_objects()

        for obj in self.objects:
            obj.size = (self.row_width, self.row_height)

        self.realized = True
        self.__update_variables_after_resize()
        self.__update_screen()


class NeonViewer(object):
    def __init__(self, gui):
        self.gui = gui
        self.mouse_x, self.mouse_y = (0, 0)
        self.mouse_down = False

        # create image
        self.img = NeonImage(self.gui)
        self.img.obj.on_mouse_down_add(self.mouse_updown_cb)
        self.img.obj.on_mouse_up_add(self.mouse_updown_cb)
        self.img.obj.on_mouse_move_add(self.mouse_move_cb)

        # setup toolbar
        self.toolbar = edje.Edje(self.gui.canvas, file=EDJE_FILE, group="image-toolbar")
        self.toolbar.size = (self.gui.win_w, BAR_HEIGHT)
        self.toolbar.part_object_get('zoom-in').on_mouse_down_add(self.img.zoom, 'in')
        self.toolbar.part_object_get('zoom-out').on_mouse_down_add(self.img.zoom, 'out')
        self.toolbar.part_object_get('zoom-fit').on_mouse_down_add(self.img.zoom, 'fit')
        self.toolbar.part_object_get('zoom-100').on_mouse_down_add(self.img.zoom, '100')
        self.toolbar.part_object_get('rotate').on_mouse_down_add(self.img.rotate)

        # setup navigation bar
        self.navbar = edje.Edje(self.gui.canvas, file=EDJE_FILE, group="image-navbar")
        self.navbar.size = (self.gui.win_w, BAR_HEIGHT)
        self.navbar.move(0, self.gui.win_h - BAR_HEIGHT)
        self.navbar.part_object_get('previous').on_mouse_down_add(self.previous_image)
        self.navbar.part_object_get('next').on_mouse_down_add(self.next_image)
        self.navbar.part_object_get('close').on_mouse_down_add(self.close)

    def mouse_move_cb(self, o, ev):
        if self.mouse_down and (self.mouse_x or self.mouse_y):
            if self.img.w <= self.gui.win_w and self.img.h <= self.gui.win_h:
                return

            delta_x = ev.position.canvas.x - self.mouse_x
            delta_y = ev.position.canvas.y - self.mouse_y
            x = self.img.x + delta_x
            y = self.img.y + delta_y

            if x > 0:
                x = 0
            if y > 0:
                y = 0
            if x < self.gui.win_w - self.img.w:
                x = -(self.img.w - self.gui.win_w)
            if y < self.gui.win_h - self.img.h:
                y = -(self.img.h - self.gui.win_h)
            self.img.move(x, y)

        self.mouse_x = ev.position.canvas.x
        self.mouse_y = ev.position.canvas.y

    def mouse_updown_cb(self, o, ev, click=False):
        if self.mouse_down:
            self.mouse_down = False
        else:
            if ev.__class__ == evas.c_evas.EventMouseDown:
                self.mouse_down = True
                self.mouse_x = ev.position.canvas.x
                self.mouse_y = ev.position.canvas.y
                if self.img.loaded:
                    if self.mouse_y < BAR_HEIGHT:
                        self.toolbar.show()
                    if self.mouse_y > self.gui.win_h - BAR_HEIGHT:
                        self.navbar.show()
                    if self.mouse_y >= BAR_HEIGHT and self.mouse_y <= self.gui.win_h - BAR_HEIGHT:
                        self.toolbar.hide()
                        self.navbar.hide()

    def previous_image(self, icon, ev):
        if self.img.index > 0:
            self.img.index -= 1
        else:
            return
        self.img.load(self.gui.fsb.images[self.img.index])

    def next_image(self, icon, ev):
        if self.img.index < len(self.gui.fsb.images) - 1:
            self.img.index += 1
        else:
            return
        self.img.load(self.gui.fsb.images[self.img.index])

    def close(self, icon, ev):
        self.img.close()
        self.toolbar.hide()
        self.navbar.hide()
        self.gui.fsb.show()

    def resize(self):
        self.toolbar.size = (self.gui.win_w, BAR_HEIGHT)
        self.navbar.size = (self.gui.win_w, BAR_HEIGHT)
        self.navbar.move(0, self.gui.win_h - BAR_HEIGHT)
        self.img.resize()
        if self.img.loaded:
            # if an image is opened when the resize occurs, filesystem browser (fsb) is raised/shown,
            # so it must be hidden.
            self.gui.fsb.hide()
            self.img.adjust()


class NeonImage(object):
    def __init__(self, gui):
        self.gui = gui
        self.win_w, self.win_h = self.gui.win.size
        self.x = self.y = 0
        self.orig_x = self.orig_y = 0
        self.w = self.h = 0

        # create an Image evas object
        self.obj = self.gui.canvas.Image()
        self.index  = None
        self.loaded = False
        self.rotated = False

        # create a Text evas object
        # used to display info about image: name, loading error, ...
        self.desc_obj = self.gui.canvas.Text(
            font         = ("sans serif", 12),
            color        = "#000000",
            style        = evas.EVAS_TEXT_STYLE_GLOW,
            shadow_color = "#cccccc",
            glow_color   = "#ffffff",
            glow2_color  = "#ffffff",
            )
        self.desc_obj.move(2, self.win_h - 18)

    def close(self):
        self.loaded = False
        self.obj.hide()
        self.desc_obj.hide()

    def load(self, path):
        if not os.access(path, os.R_OK):
            return

        self.loaded = True
        self.rotated = False
        self.desc_obj.text_set('Loading...')
        self.desc_obj.show()
        self.gui.canvas.render()
        try:
            self.obj.file_set(path)
        except:
            self.desc_obj.text_set('Error: failed to load image %s' % path)
            return

        self.w, self.h = self.obj.image_size_get()
        self.orig_w, self.orig_h = (self.w, self.h)
        self.adjust()

        self.desc_obj.text_set(path)
        self.desc_obj.raise_()

        self.show()

    def move(self, x, y):
        self.x = x
        self.y = y
        self.obj.fill_set(min(0, self.x), min(0, self.y), self.w, self.h)

    def resize(self):
        self.win_w, self.win_h = self.gui.win.size
        self.desc_obj.move(2, self.win_h - 18)

    def rotate(self, icon, ev):
        if not self.rotated:
            self.obj.rotate(evas.EVAS_IMAGE_ROTATE_270)
        else:
            self.obj.rotate(evas.EVAS_IMAGE_ROTATE_90)
        self.rotated = not self.rotated
        self.w, self.h = self.obj.image_size_get()
        self.orig_w, self.orig_h = (self.w, self.h)
        self.adjust()

    def show(self):
        self.obj.show()

    def adjust(self):
        if self.w <= self.win_w and self.h <= self.win_h:
            self.zoom(None, None, '100')
        else:
            self.zoom(None, None, 'fit')

    def zoom(self, icon, ev, type):
        if type == 'fit':
            # zoom best fit (fit image to window)
            zoom_factor = min(
                self.win_w / float(self.w),
                self.win_h / float(self.h)
                )
            self.w = int(self.w * zoom_factor)
            self.h = int(self.h * zoom_factor)
            self.x = (self.win_w - self.w) / 2
            self.y = (self.win_h - self.h) / 2
        elif type == '100':
            # zoom 100% (1:1)
            self.w, self.h = self.obj.image_size_get()
            self.x = (self.win_w - self.w) / 2
            self.y = (self.win_h - self.h) / 2
        elif type == 'in' or  type == 'out':
            # zoom in/out
            #
            # for example with an increment of 0.5
            #                     1   1   1   1
            # 3  2.5  2  1.5  1  ---  -  ---  -
            #                    1.5  2  2.5  3
            #
            inc = 0.4
            zoom_factor = self.w / float(self.orig_w)
            reverse = False
        
            if type == 'out':
                inc = -inc
            if zoom_factor < 1:
                inc = -inc
                zoom_factor = 1 / float(zoom_factor)
                reverse = True
            if zoom_factor + inc < 1:
                zoom_factor += inc
                zoom_factor = 2 - zoom_factor
                reverse = not reverse
            else:
                zoom_factor += inc
            if reverse:
                zoom_factor = 1 / float(zoom_factor)

            old_w = self.w
            old_h = self.h
            self.w = self.orig_w * zoom_factor
            self.h = self.orig_h * zoom_factor
            if self.w > self.win_w:
                center_x = (-self.x + self.win_w / 2.0) * (self.w / float(old_w))
                self.x = -(center_x - self.win_w / 2.0)
                if self.x > 0:
                    self.x = 0
                if self.x < self.win_w - self.w:
                    self.x = -(self.w - self.win_w)
            else:
                self.x = (self.win_w - self.w) / 2.0
            if self.h > self.win_h:
                center_y = (-self.y + self.win_h / 2.0) * (self.h / float(old_h))
                self.y = -(center_y - self.win_h / 2.0)
                if self.y > 0:
                    self.y = 0
                if self.y < self.win_h - self.h:
                    self.y = -(self.h - self.win_h)
            else:
                self.y = (self.win_h - self.h) / 2

        self.obj.move(max(0, self.x), max(0, self.y))
        self.obj.resize(min(self.win_w, self.w), min(self.win_h, self.h))
        self.obj.fill_set(min(0, self.x), min(0, self.y), self.w, self.h)


class NeonGui(object):
    def __init__(self):
        edje.frametime_set(1 / 20.0)
        ecore.animator_frametime_set(1 / 20.0)

        self.win = ecore.evas.SoftwareX11(w=WIN_WIDTH, h=WIN_HEIGHT)
        self.win.title = APP_NAME
        # call function on_resize() when window is resized so we can handle resizing of all objects
        self.win.callback_resize = self.on_resize
        self.win.callback_delete_request = self.quit
        #self.win.maximized_set(True)
        #self.win.direct_resize_set(True)

        # get drawing area created by SoftwareX11
        self.canvas = self.win.evas

        self.win_w, self.win_h = self.win.size

        self.edje_main = edje.Edje(self.canvas, file=EDJE_FILE, group="main")
        self.edje_main.size = self.win.size
        self.edje_main.show()

        # create the filesystem browser
        self.fsb = NeonFsb(self)
        # create the image viewer
        self.viewer = NeonViewer(self)

        # FIXME
        self.bg = self.edje_main.part_object_get('background')
        self.bg.on_mouse_down_add(self.viewer.mouse_updown_cb)
        self.bg.on_mouse_up_add(self.viewer.mouse_updown_cb)
        self.bg.on_key_down_add(self.on_key_down)
        self.bg.focus = True

        # show X11 window created by SoftwareX11
        self.win.show()

    def on_resize(self, obj):
        self.win_w, self.win_h = self.win.size
        self.edje_main.size = self.win.size
        self.viewer.resize()

    def on_key_down(self, obj, event):
        if event.keyname in ("F11", "f"):
            self.toggle_fullscreen()
        elif event.keyname in ("Escape", "q" , "e"):
            self.quit(None)

    def toggle_fullscreen(self, *a):
        self.win.fullscreen = not self.win.fullscreen

    def quit(self, o, *a):
        print "Bye bye, thx to use %s" % APP_NAME
        ecore.main_loop_quit()


if __name__ == "__main__":
    path = sys.argv[1] if len(sys.argv) > 1 else os.path.expanduser("~")
    path = os.path.abspath(path)

    gui = NeonGui()
    gui.fsb.load(path)

    # enter main loop and wait until window is closed
    ecore.main_loop_begin()
